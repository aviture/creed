# Aviture Creed
The purpose of this project is to document Aviture's creed.  What is the creed?  Let's start with the classical definition of the word:
> __creed__  
> *n.*  
> any system or codification of belief or of opinion

Sounds good  - let's roll with that.  We'll call this the codification of the opinions and beliefs that guide our company and our day-to-day approach to the work that we do.  *(i.e. This document describes what we are all about.)*  

Hold to these beliefs and you'll do fine here.

Why is this in Bitbucket instead of some snazzy Wiki or CMS solution?  Because we are a software development firm.  We live here.  It makes sense to codify our code in code!
## Solutions over Technology and Features ![](http://latex.codecogs.com/gif.latex?%5Cleft%20%28%5Cfrac%7BSolutions%7D%7BTechnology&plus;Features%7D%5Cright%20%29)  

Our focus is on **solving problems**, not building fancy features with technology.  Don't get me wrong - we build plenty of cool features using awesome technology but it isn't our focus or what we are about.  This may sound odd to many people, specifically those familiar with our work.  As a modern software development company, why not focus on technology and features first?  Aren't those cool features and the cutting-edge technologies the reason people get excited about Aviture?

The answer is simple: Well-built incorrect solutions fail.  *We don't participate in failure.*  Typical software development focuses on determining the correct way to build something.  *We are not typical.*  We focus on understanding the needs of the customer.  Often times what the customer initially asks for is not what they truly need.  If the correct solution is not developed it will not satisfy the customer's needs - no matter how well constructed it is.  We put significant effort into understanding and documenting the problems faced by our customers whether they are stated, unstated or even unrecognized.   This understanding frames the prototype and experimentation that we do towards solution approaches.  Only when an approach is validated as an effective solution within the context of the problem do we invest the time and effort into hardening that solution.

*tl;dr - We ensure our solutions are the correct solutions - then we add the awesome sauce!*
#####Relevant Things:
* [User Experience Research](http://chaione.com/what-is-ux-research-and-why-should-i-care/)
* [Behavior Driven Development](http://dannorth.net/introducing-bdd/)
* [Test Driven  Development (to include ATDD)](http://agiledata.org/essays/tdd.html)

## Freedom and Creativity
We believe that creativity is essential in developing frickin awesome applications.  Strive to be different and to walk the path unseen.  Understand that a fundamental condition required for creativity to flourish is freedom. Exploration, imagination, experimentation, expression - none of these can thrive without freedom.

We want you to be free. We want you to be brave.  Try something different.  Pursue wild and unique ideas.  Establish a pace and a path in which you don't follow a straight line to get to your goal.  Dare to explore as you travel to your destination.  Surprise us and demonstrate that you are not a number. 
#####Relevant Things:
* [Set your mind to experimentation mode](https://www.rallydev.com/community/agile/set-your-mind-experimentation-mode)

## Empowerment and Accountability
Do what is right.  We trust you.  We brought you onboard because we feel you have 'the right stuff'.  We don't expect you to be perfect, but we hope you'll be courageous.  The only true failure is that of not trying.

Own your commitments.  Own your successes.  Own your failures.  When something goes wrong do what it takes to make it right.  When you experience success share it and help us to replicate it.  Take pride in your work, but ask for aid or guidance when you feel it's needed.  Tell us what you need to be successful and we'll do our best to give it to you. 

Don't wait on anyone.  You have our permission to kick ass.
#####Relevant Things:
* [Scrum Master as a Servant Leader](http://agile.dzone.com/articles/scrum-master-servant)
* [Joel on Software's view on Servant Leadership](http://www.inc.com/magazine/20081201/how-hard-could-it-be-my-style-of-servant-leadership.html?partner=fogcreek)

## Growth and Enrichment
Never be content - there is always more to know.  Don't be afraid of failure, embrace it and the knowledge gained through it.  Keep trying until you get it right.  Don't stop.  Don't give up.

Learn new technologies.  Learn new business skills.  Follow the latest trends.  Pursue the insights of the innovators and leaders today.  Understand the 'why' behind the 'what'.  Try to predict what's next.

Reach out to your comrades - both at work and in the community.  Share, instruct and advise based on your experience.  Learn, explore and understand based on the experiences of others.  
#####Relevant Things:
* [Fail Early & Often](http://blog.codinghorror.com/fail-early-fail-often/)

## Fun and Family
Keep the spark alive.  Protect enough time to always be pursuing something that excites you at work.  Find something to look forward to every day.  Raise the spirits of those around you and focus on positivity.  Keep fun as part of the equation and you'll always stay fresh.

Work reasonable hours.  Protect enough time to live life.  Be there for your significant other.  Be there for your children.  Be there for your friends.

Make your coworkers part of an extended family.  Build friendships, celebrate successes, provide support in times of need.  We are all part of a team.
#####Relevant Things:
* [Sustainable Pace](https://www.scrumalliance.org/community/articles/2011/december/sustainable-pace-trusting-your-teams)

## Common Sense
Quick story - When we first acquired our own building we decided to provide beer because to be honest, we like beer.  We had some initial discussions surrounding what rules and guidelines we'd need surrounding the consumption of alcohol at work.  As you can imagine, the rules surrounding this could quickly become complicated and messy in order to ensure that every scenario was appropriately covered.  In the end we created one rule, which we have dubbed the 'keg rule':
> **"Don't be stupid"** - the keg rule

This is the 'golden rule' at Aviture and governs a majority of the things we do in-house beyond beer consumption.  As a company we strive to be pragmatic and to keep things simple.  We hate rules, sub-rules, caveats to sub-rules and processes to govern the exception to the caveats to sub-rules via ballot of committee members.  We rely on people using common sense and having open discussions to avoid the exercise of trying to define exactly what common sense is.

Please don't make us write needless rules - we'd rather be solving problems for our customers.

#####Relevant Things:
* [The Pramatic Agile Manifesto](https://www.scrumalliance.org/community/articles/2013/2013-april/what-does-the-agile-manifesto-mean)

##Kick Ass
Need I say more?  Go forth and show us what you are made of!
